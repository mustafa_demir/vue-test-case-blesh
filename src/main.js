import Vue from "vue";
import App from "./App.vue";
import * as VueGoogleMaps from "vue2-google-maps";
import VueResource from "vue-resource";
import { store } from "./store/store";
import "bootstrap"; 
import "bootstrap/dist/css/bootstrap.min.css";
require("vue-flash-message/dist/vue-flash-message.min.css");
import VueFlashMessage from "vue-flash-message";
import CONST from "./veriables/const";
import { router } from "./router";

Vue.use(VueFlashMessage);

Vue.use(VueResource);
/*Vue.http.headers.common['Access-Control-Allow-Origin'] = true
Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'
Vue.http.options.emulateJSON = false
Vue.http.options.credentials = false
Vue.http.options.withCredentials = false
Vue.http.options.xhr = {withCredentials: false}*/

Vue.use(VueGoogleMaps, {
  load: {
    key: CONST.GOOGLE_JS_API.KEY,
    libraries: "places" // necessary for places input
  }
});

new Vue({
  el: '#app',
  render: h => h(App),
  store,
  router
})
