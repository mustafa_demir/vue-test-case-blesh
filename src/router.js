import Vue from "vue";
import VueRouter from "vue-router";
import BeaconTable from "./components/BeaconTable";
import GoogleMap from "./components/GoogleMap";


Vue.use(VueRouter);

const routes = [
    { path : "/", component :  GoogleMap },
    { path : "/table", component :  BeaconTable },
    { path : "*", redirect : "/" },
];

export const router = new VueRouter({
    mode : "history",
    routes 
})