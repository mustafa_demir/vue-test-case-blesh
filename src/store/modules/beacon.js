import Vue from "vue";
import CONST from "../../veriables/const.js";

const state = {
    beacons : []
}

const getters = {
    getBeacons(state){
        //console.log("getters" + state.beacons[state.beacons.length-1]);
        return state.beacons[state.beacons.length-1]; // last state
    }
}

const mutations = {
    updateBeacons(state, beacons){
        state.beacons.push(beacons);
    }
}

const actions = {
    fethcBeacons({commit, state }) {
        Vue.http.post(CONST.BLESH_API.BEACON_LIST_PATH, { 
                accessToken : CONST.BLESH_API.ACCESS_TOKEN
            },
            { emulateJSON: true }
        ) 
        .then(response => {
          commit("updateBeacons", response.data.beacons); // update state
       })
      .catch(e=>console.log(e));

    },
    updateBeaconsDesc({commit, state }, payload) {
        return new Promise((resolve, reject) => {

            let currentBeacons = payload.beaconList;
            let index = payload.index;
            let devId = currentBeacons[index].id;
            let desc = payload.description;
        
            Vue.http.post(CONST.BLESH_API.BEACON_UPDATE_PATH, { 
                    accessToken : CONST.BLESH_API.ACCESS_TOKEN,
                    deviceId: devId,
                    description : desc
                },
                { emulateJSON: true }
            ) 
            .then(response => {
                resolve(response);
                if(response.data.resultCode == 0) {
                    currentBeacons[index].description = desc;
                    commit("updateBeacons", currentBeacons); // update state
                
                }
        })
        .catch(e=>reject(e));
      })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}