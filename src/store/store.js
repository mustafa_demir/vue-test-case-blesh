import Vue from "vue";
import Vuex from "vuex";
import beacon from "./modules/beacon";
import * as getters from "./getters"; 
import * as mutations from "./mutations"; 
import * as actions from "./actions"; 

Vue.use(Vuex);
export const store  = new Vuex.Store({
    state: {   
    },
    modules : {
        beacon
    }
});